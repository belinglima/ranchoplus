/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import rancho.database.DatabaseOperator;
import rancho.objects.Pedido;
import rancho.objects.PedidoFornecedor;
import rancho.objects.Produto;

/**
 *
 * @author frederico
 */
public class testDatabaseOperator {
    
    private DatabaseOperator databaseOperator = null;
    
    public testDatabaseOperator() throws FileNotFoundException, SQLException {
        
        databaseOperator = new DatabaseOperator();
        
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void testarBuscarProdutos() throws SQLException{
        
        // testando tipo de retorno da busca de produtos
        
        System.out.println("Testando busca de produtos ...");
        
        ArrayList<Produto> produtos = databaseOperator.buscarProdutos(0);
        
        for (int i=0; i<produtos.size(); i++){
            
            assertTrue(produtos.get(i) instanceof Produto);
            
        }
        
    }
    
    @Test
    public void testarBuscarPedidosCliente() throws SQLException{
        
        // testando tipo de retorno da busca de produtos
        
        System.out.println("Testando busca de pedidos (cliente) ...");
        
        ArrayList<Pedido> pedidos = databaseOperator.buscarPedidos("fred.s.kremer@gmail.com");
        
        for (int i=0; i<pedidos.size(); i++){
            
            assertTrue(pedidos.get(i) instanceof Pedido);
            
        }
        
    }

    @Test
    public void testarBuscarPedidosFornecedor() throws SQLException{
        
        // testando tipo de retorno da busca de produtos
        
        System.out.println("Testando busca de pedidos (fornecedor) ...");
        
        ArrayList<PedidoFornecedor> pedidos = databaseOperator.listarPedidosFornecedor("contato@guanabara.com.br");
        
        for (int i=0; i<pedidos.size(); i++){
            
            assertTrue(pedidos.get(i) instanceof Pedido);
            
        }
        
    }
}
