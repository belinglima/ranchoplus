/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rancho.controllers;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import rancho.database.DatabaseOperator;
import rancho.objects.Pedido;

/**
 *
 * @author frederico
 */


@RestController
public class VerPedidosController {

    
    @RequestMapping("/listarPedidos")
    public ArrayList<Pedido> mostrarPedidos(@RequestParam(value="username", defaultValue="") String username) throws FileNotFoundException, SQLException {
                
        DatabaseOperator database = new DatabaseOperator();
        
        return database.buscarPedidos(username);
        
    
    }

}
