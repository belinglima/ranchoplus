package rancho.controllers;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import rancho.objects.Cadastro;
import rancho.database.DatabaseOperator;
import rancho.objects.PedidoFornecedor;
import rancho.objects.Produto;

@RestController
public class FornecedorProdutosController {    

    @RequestMapping("/listarMeusProdutos")
    public ArrayList<Produto> listarMeusProdutos(@RequestParam(value="fornecedor") String fornecedor) throws FileNotFoundException, SQLException {
                
        DatabaseOperator database = new DatabaseOperator();
        
        ArrayList<Produto> produtos = new ArrayList();
        
        produtos = database.listarMeusProdutos(fornecedor);
        
        return produtos;
        
    }

    @RequestMapping("/deletarProduto")
    public Cadastro deletarProduto(@RequestParam(value="id") int id,
                                             @RequestParam(value="fornecedor") String fornecedor) throws FileNotFoundException, SQLException {
                
        DatabaseOperator database = new DatabaseOperator();
        
        boolean resposta = database.deletarProdutoFornecedor(id, fornecedor);
        
        if (resposta){
            
            return new Cadastro("ok", "registro deletado");
            
        } else {
            
            
            return new Cadastro("erro", "não foi possivel remover o registro");
            
        }
        
    }

    @RequestMapping("/cadastrarProduto")
    public Cadastro listarMeusProdutos(@RequestParam(value="fornecedor") String fornecedor,
                                      @RequestParam(value="nome") String nome,
                                      @RequestParam(value="identificador") String identificador,
                                      @RequestParam(value="preco") double preco,
                                      @RequestParam(value="fotoURL") String fotoURL) throws FileNotFoundException, SQLException {
                
        DatabaseOperator database = new DatabaseOperator();
        
        ArrayList<Produto> produtos = new ArrayList();
        
        boolean resposta = database.cadastrarProduto(fornecedor, nome, identificador, preco, fotoURL);
        
        if (resposta){
            
            return new Cadastro("ok", "produto cadastrado com sucesso");
            
        } else {
            
            return new Cadastro("erro", "erro ao cadastrar o produto");
            
        }
        
    }
    
        @RequestMapping("/PedidosFornecedor")
            public ArrayList<PedidoFornecedor> listarPedidosFornecedor(@RequestParam(value="fornecedor") String fornecedor) throws FileNotFoundException, SQLException {
                
        DatabaseOperator database = new DatabaseOperator();
        
        return database.listarPedidosFornecedor(fornecedor);

        
        
    }

}