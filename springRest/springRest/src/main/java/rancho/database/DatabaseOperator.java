/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rancho.database;

import rancho.database.Database;
import rancho.objects.Produto;
import rancho.objects.Pedido;
import rancho.objects.PedidoFornecedor;
import rancho.objects.Cadastro;
import rancho.objects.Cliente;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Set;
import rancho.objects.Config;
import rancho.objects.Endereco;

/**
 *
 * @author frederico
 */
public class DatabaseOperator  {
    
    Database database = null;
    
    public DatabaseOperator() throws FileNotFoundException, SQLException {
    
        Gson gson = new Gson();
        
        BufferedReader reader = new BufferedReader(new FileReader("/home/frederico/Documents/senac/2_semestre/Algoritmos_E_Programação_II/ranchoplus/springRest/src/main/java/rancho/config.json"));
        Config configuration = gson.fromJson(reader, Config.class);
        String dbname = configuration.getDatabaseName();
        String username = configuration.getDatabaseUsername();
        String password = configuration.getDatabasePassword();
        
        
        this.database = new Database(dbname, username, password);
        this.database.Connect();
        
    }
    
    public boolean emailCadastrado(String email) throws SQLException{
        
        // checar administradores
        
        Statement busca = this.database.createStatement();
        ResultSet resultado = busca.executeQuery("SELECT * FROM administradores WHERE email = '" + email + "'");
        
        while (resultado.next()){
            
            return true;
            
        }
        
        // checar clientes
        
        busca = this.database.createStatement();
        resultado = busca.executeQuery("SELECT * FROM administradores WHERE email = '" + email + "'");
        
        while (resultado.next()){
            
            return true;
            
        }  
        
        // checar fornecedores
        
        busca = this.database.createStatement();
        resultado = busca.executeQuery("SELECT * FROM fornecedores WHERE email = '" + email + "'");
        
        while (resultado.next()){
            
            return true;
            
        }  
        
        return false;        
        
    }

    public boolean usernameCadastrado(String username) throws SQLException{
        
        // checar administradores
        
        Statement busca = this.database.createStatement();
        ResultSet resultado = busca.executeQuery("SELECT * FROM administradores WHERE username = '" + username + "'");
        
        while (resultado.next()){
            
            return true;
            
        }
        
        // checar clientes
        
        busca = this.database.createStatement();
        resultado = busca.executeQuery("SELECT * FROM administradores WHERE username = '" + username + "'");
        
        while (resultado.next()){
            
            return true;
            
        }  
        
        // checar fornecedores
        
        busca = this.database.createStatement();
        resultado = busca.executeQuery("SELECT * FROM fornecedores WHERE username = '" + username + "'");
        
        while (resultado.next()){
            
            return true;
            
        }  
        
        return false;        
        
    }

    
    public boolean checarCadastroAdministrador(String email, String password) throws SQLException {
        
        Statement busca = this.database.createStatement();
        ResultSet resultado = busca.executeQuery("SELECT * FROM administradores WHERE email = '" + email + "' AND password ='"+password+"'");
        
        while (resultado.next()){
            
            return true;
            
        }
        
        return false;
        
    }
    
    public boolean checarCadastroCliente(String email, String password) throws SQLException{
        
        Statement busca = this.database.createStatement();
        ResultSet resultado = busca.executeQuery("SELECT * FROM clientes WHERE email = '" + email + "' AND password ='"+password+"'");
        
        while (resultado.next()){
            
            return true;
            
        }
        
        return false;
        
    }
        
    public boolean checarCadastroFornecedor(String email, String password) throws SQLException{
        
        Statement busca = this.database.createStatement();
        ResultSet resultado = busca.executeQuery("SELECT * FROM fornecedores WHERE email = '" + email + "' AND password ='"+password+"'");
        
        while (resultado.next()){
            
            return true;
            
        }
        
        return false;
        
    }        
    
    
    public boolean cadastrarAdministrador(String usuario, String email, String password, String nome,
                                          String cpf, String rg,
                                          String cargo, String setor) throws SQLException{
        
        Statement cursor = this.database.createStatement(); 
        cursor.execute(String.format("INSERT INTO administradores (usuario, email, password, nome, cpf, rg, cargo, setor) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s') ", usuario, email, password, nome, cpf, rg, cargo, setor));
        
        return false;
        
    }
    
    public boolean cadastrarCliente(String usuario, String email, String password, String nome,
                                          String cpfCnpj, String rg,
                                          String tipo) throws SQLException{
        
        
        
        
        Statement cursor = this.database.createStatement(); 
        
        String sql = String.format("SELECT * FROM clientes WHERE email = '%s'", email);
        
        ResultSet resultado = cursor.executeQuery(sql);
        
        while (resultado.next()){
            
            return false;
            
        }
        
        cursor = this.database.createStatement(); 
        
        sql = (String.format("INSERT INTO clientes (username, email, password, nomeCompleto, cpfCnpj, rg, tipoDePessoa) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s') ", usuario, email, password, nome, cpfCnpj, rg, tipo));
        cursor.execute(sql);
        return true;    
    
    }
    
    public boolean cadastrarFornecedor(String usuario, String email, String password, String razaoSocial,
                                          String cnpj) throws SQLException{
    
        
      Statement cursor = this.database.createStatement(); 
        
        String sql = String.format("SELECT * FROM clientes WHERE email = '%s'", email);
        
        ResultSet resultado = cursor.executeQuery(sql);
        
        while (resultado.next()){
            
            return false;
            
        }
        
        cursor = this.database.createStatement(); 
        cursor.execute(String.format("INSERT INTO fornecedores (username, email, password, razaoSocial, cnpj) VALUES ('%s', '%s', '%s', '%s', '%s') ", usuario, email, password, razaoSocial, cnpj));
        
        return true;    
    
    } 

    public ArrayList<Produto> buscarProdutos(int numero) throws SQLException{
        
        ArrayList<Produto> produtos = new ArrayList();
        
        try {
        
            Statement cursor = this.database.createStatement(); 
            String sql = String.format("SELECT produtos.nome AS produto, "+
                                       "       fornecedores.razaoSocial AS fornecedor,"+
                                       "       produtos.fotoURL as fotoURL,"+
                                       "       produtosDosFornecedores.estoque AS quantidade,"+
                                       "       produtosDosFornecedores.precoUnitario AS valor, "+
                                       "       produtosDosFornecedores.id AS idProdutoDoFornecedor "+
                                       "FROM produtos, produtosDosFornecedores, fornecedores "+
                                       "WHERE produtos.id = produtosDosFornecedores.produtoId AND fornecedores.id = produtosDosFornecedores.fornecedor "+
                                       "ORDER BY produtosDosFornecedores.precoUnitario ");
            ResultSet resultado = cursor.executeQuery(sql);

            while (resultado.next()){
                //String nome, String fornecedor, String fotoURL, String categoria, int quantidade, double valor
                Produto produto = new Produto(resultado.getString("produto"), 
                                              resultado.getString("fornecedor"),
                                              resultado.getString("fotoURL"),
                                              "",
                                              resultado.getInt("quantidade"),
                                              resultado.getDouble("valor"),
                                              resultado.getInt("idProdutoDoFornecedor"));
                produtos.add(produto);

            }

            return produtos;
        
        } catch (SQLException error){
            
            System.out.println(error.getMessage());
                        
            return produtos;
            
        }
    } 

    public Produto buscarProduto(int id) throws SQLException {
        
            Statement cursor = this.database.createStatement(); 
            String sql = String.format("SELECT produtos.nome AS produto, "+
                                       "       fornecedores.razaoSocial AS fornecedor,"+
                                       "       produtos.fotoURL as fotoURL,"+
                                       "       produtosDosFornecedores.estoque AS quantidade,"+
                                       "       produtosDosFornecedores.precoUnitario AS valor, "+
                                       "       produtosDosFornecedores.id AS idProdutoDoFornecedor "+
                                       "FROM produtos, produtosDosFornecedores, fornecedores "+
                                       "WHERE produtos.id = produtosDosFornecedores.produtoId "+
                                       "AND fornecedores.id = produtosDosFornecedores.fornecedor "+
                                       "AND produtosDosFornecedores.id = %s", id);
            
            ResultSet resultado = cursor.executeQuery(sql);
            
            while (resultado.next()){
                
                return new Produto(resultado.getString("produto"), 
                                              resultado.getString("fornecedor"),
                                              resultado.getString("fotoURL"),
                                              "",
                                              resultado.getInt("quantidade"),
                                              resultado.getDouble("valor"),
                                              resultado.getInt("idProdutoDoFornecedor"));                
                
            }
            
            return null;
        
    }
    
    public boolean adicionarProdutoAoCarrinho(String cliente, String produto, String quantidade){
        
        try  {
            
            Statement cursor = this.database.createStatement();
            
            ResultSet resultado = cursor.executeQuery("SELECT id FROM clientes WHERE email='"+cliente+"'");
            
            while(resultado.next()){
                
                String id = resultado.getString("id");
                
                
                
                cursor.execute(String.format("INSERT INTO itensNoCarrinhos (quantidade, data, cliente, produto) " +
                                             "    VALUES (%s, CURDATE(), %s, %s)", quantidade, id, produto));
                
                return true;
                
            }
            
            return false;
            
            
        } catch (SQLException ex) {
            
            System.out.println(ex.getMessage());
            
            return false;
        
        }
        
    }


    public ArrayList<Produto> buscarProdutosCarrinho(String cliente) throws SQLException {
        
        ResultSet resultado = null;
        Statement cursor = this.database.createStatement(); 
        Statement cursor2 = this.database.createStatement();
        String sql = String.format("SELECT produto, quantidade FROM itensNoCarrinhos WHERE itensNoCarrinhos.cliente IN (select id from clientes where email ='%s')", cliente);
        resultado = cursor.executeQuery(sql);
        
        ArrayList<Produto> produtos = new ArrayList();
        
        while(resultado.next()){
            
            Produto produto = this.buscarProduto(resultado.getInt("produto"));
            produto.setQuantidade(resultado.getInt("quantidade"));
            
            
            produtos.add(produto);
            
        }
        
        return produtos;
        
    }

    public Cadastro limparCarrinho(String cliente) throws SQLException {
        
        ResultSet resultado = null;
        Statement cursor = this.database.createStatement(); 
        Statement cursor2 = this.database.createStatement();
        String sql = String.format("DELETE FROM itensNoCarrinhos WHERE itensNoCarrinhos.cliente IN (SELECT id FROM clientes WHERE email = '%s')", cliente);
        try {
            
           cursor.execute(sql); 
           return new Cadastro("ok", "foi possivel remover os itens");
            
        } catch (SQLException ex){
            
            return new Cadastro("error", "nao foi possivel remover os itens");
            
        }
        
    }

    
    public int cadastrarEndereco(String uf, String cidade, String cep, String rua, String numero, String complemento, String bairro) throws SQLException{
        
        
        Statement cursor = this.database.createStatement();
        String sql = String.format("INSERT INTO enderecos (estado, cidade, cep, logradouro, numero, complemento, bairro) " +
                                       "VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s')",
                                       uf, cidade, cep, rua, numero, complemento, bairro);
        
        cursor.execute(sql);
        
        ResultSet resultado = null;
        
        sql = String.format("select id from enderecos WHERE cep='%s' AND  numero='%s' AND  complemento='%s'", cep, numero, complemento);
        
        resultado = cursor.executeQuery(sql);
        
        while (resultado.next()){
            
            
            return resultado.getInt("id");
            
        }
        
        return 0;
        
    }
    
    
    public int clienteId(String email) throws SQLException{
        
        Statement cursor = this.database.createStatement();
        String sql = String.format("select id from clientes WHERE email='%s'", email);
                
        ResultSet resultado = cursor.executeQuery(sql);
        
        while (resultado.next()){
            
            
            return resultado.getInt("id");
            
        }        
        
        return 0;
        
    }
    
    public boolean confirmarPedido(int clienteId, int enderecoId) throws SQLException{

        String sql = "";
        int pedidoId = 0;
        ResultSet resultado = null;
        Statement cursor = this.database.createStatement();
        
        
        
        double valorPedido = 0;
        
        sql = String.format("SELECT SUM(quantidade * produtosDosFornecedores.precoUnitario) AS total FROM itensNoCarrinhos, produtosDosFornecedores WHERE itensNoCarrinhos.cliente =%s AND itensNoCarrinhos.produto = produtosDosFornecedores.id;", clienteId);
                
        resultado = cursor.executeQuery(sql);
        while(resultado.next()){
            
            valorPedido = resultado.getDouble("total");
            
        }
        
        
        sql = String.format("INSERT INTO pedidos (cliente, valor, status, data, endereco) VALUES (%s, %s,'aguardado resposta', CURDATE(), %s)", clienteId, valorPedido, enderecoId);
                
        try {
            
            cursor.execute(sql);
            
        } catch (SQLException ex){
            
            System.out.println(ex.getMessage());
            return false;
            
        }
        
        sql = String.format("SELECT id FROM pedidos WHERE cliente = %s ORDER BY id DESC LIMIT 1", clienteId);
                
        resultado = cursor.executeQuery(sql);
        
        while(resultado.next()){
            
            pedidoId = resultado.getInt("id");
            break;
            
        }
        
        
        
        sql = String.format("SELECT * FROM itensNoCarrinhos WHERE cliente = %s", clienteId);
                
        resultado = cursor.executeQuery(sql);
        
        cursor = this.database.createStatement();
        
        while(resultado.next()){
            
            sql = String.format("INSERT INTO itensDosPedidos (pedido, produto, quantidade) VALUES (%s, %s, %s)", pedidoId, 
                                                                                                                  resultado.getInt("produto"),
                                                                                                                  resultado.getInt("quantidade"));
            
                        
            cursor.execute(sql);
            
            sql = String.format("DELETE FROM itensNoCarrinhos WHERE id = %s", resultado.getInt("id"));
            
            
            cursor.execute(sql);
            
        }
        
        return true;
        
    }
    
    public ArrayList<Pedido> buscarPedidos(String cliente) throws SQLException{
        
        ArrayList<Pedido> pedidos = new ArrayList();
        
        Statement cursor = this.database.createStatement();
        
        ResultSet resultado = null;
        
        String sql = String.format("SELECT * FROM pedidos WHERE cliente IN (SELECT id FROM clientes WHERE email = '%s')", cliente);
        resultado = cursor.executeQuery(sql);
        
        while(resultado.next()){
            
            ArrayList<Produto> produtos = new ArrayList();
            Pedido pedido = new Pedido(produtos, resultado.getInt("id"), resultado.getDate("data"), 0);
            pedidos.add(pedido);
            
        }
        

        for (int i=0; i< pedidos.size(); i++){
            
            cursor = this.database.createStatement();
            resultado = null;            
            sql = String.format("SELECT produtos.nome AS nome, " +
                                       "fornecedores.razaoSocial AS fornecedor, " +
                                       "itensDosPedidos.quantidade AS quantidade, " +
                                       "produtosDosFornecedores.precoUnitario AS valor " +
                                "FROM produtos, itensDosPedidos, produtosDosFornecedores, fornecedores " +
                                "WHERE itensDosPedidos.pedido = %s " +
                                    "AND itensDosPedidos.produto = produtosDosFornecedores.id " +
                                    "AND fornecedores.id = produtosDosFornecedores.fornecedor " +
                                    "AND produtos.id = produtosDosFornecedores.produtoId", 
                                pedidos.get(i).getId());
            resultado = cursor.executeQuery(sql);
            
            while(resultado.next()){
                
                pedidos.get(i).addProduto(new Produto(resultado.getString("nome"), 
                                               resultado.getString("fornecedor"), 
                                               "", "", 
                                               resultado.getInt("quantidade"), 
                                               resultado.getInt("quantidade") * resultado.getDouble("valor"), 0));
                
                pedidos.get(i).setTotal(pedidos.get(i).getTotal() + resultado.getInt("quantidade") * resultado.getDouble("valor"));
                
            }
       
            
        };
        
        
        return pedidos;
        
    }

    public ArrayList<Produto> listarMeusProdutos(String fornecedor) throws SQLException{
        
        ArrayList<Produto> produtos = new ArrayList();
        
        Statement cursor = this.database.createStatement();
        ResultSet resultados = null;
        
        String sql = String.format("SELECT produtosDosFornecedores.id AS id, produtos.nome AS nome, " +
                                       "produtos.identificadorUniversal AS identificadorUniversal, " +
                                       "produtosDosFornecedores.precoUnitario AS precoUnitario " +
                                   "FROM produtosDosFornecedores, produtos " +
                                   "WHERE fornecedor IN (SELECT id FROM fornecedores WHERE email = '%s') " +
                                        "AND produtos.id = produtosDosFornecedores.produtoId", fornecedor);
        
        resultados = cursor.executeQuery(sql);
        
        while(resultados.next()){
            
            Produto produto = new Produto(resultados.getString("nome"), 
                                          "", 
                                          "", 
                                          resultados.getString("identificadorUniversal"), 
                                          0, 
                                          resultados.getDouble("precoUnitario"), 
                                          resultados.getInt("id"));
            
            produtos.add(produto);
            
        }
                
        return produtos;
        
    }

    public boolean deletarProdutoFornecedor(int id, String fornecedor) throws SQLException {
        
        Statement cursor = this.database.createStatement();
        String sql = String.format("DELETE FROM produtosDosFornecedores WHERE id = %s AND fornecedor IN (SELECT id FROM fornecedores WHERE email = '%s')", id, fornecedor);
        
        try {
            
            cursor.execute(sql);
            
            return true;
            
        } catch (SQLException ex) {
            
            System.out.println(ex.getMessage());
            return false;
            
        }
        
        
    }

    public boolean cadastrarProduto(String fornecedor, String nome, String identificador, double preco, String fotoURL) throws SQLException {
        
        Statement cursor = this.database.createStatement();
        ResultSet resultado = null;
        String sql;
        int produtoId = 0;
        int fornecedorId = 0;
        
        // recuperar id do fornecedor
        
        resultado = cursor.executeQuery(String.format("SELECT id FROM fornecedores WHERE email = '%s'", fornecedor));
    
        while(resultado.next()){
            
            fornecedorId = resultado.getInt("id");
            break;
            
        }
        
        // checar de o produto ja esta cadastrado
        
        cursor = this.database.createStatement();
        resultado = null;
        boolean produtoCadastrado = false;
        sql = String.format("SELECT id FROM produtos WHERE identificadorUniversal='%s'", identificador);
        
        resultado = cursor.executeQuery(sql);
        
        while(resultado.next()){
            
            produtoCadastrado = true;
            produtoId = resultado.getInt("id");
            
        }
        
        if (!produtoCadastrado){
            
            
            sql = String.format("INSERT INTO produtos (identificadorUniversal, nome, fotoURL) VALUES ('%s','%s','%s')", identificador, nome, fotoURL);
            cursor = this.database.createStatement();
            cursor.execute(sql);
            
            sql = String.format("SELECT id FROM produtos WHERE identificadorUniversal='%s'", identificador);
            resultado = cursor.executeQuery(sql);

            while(resultado.next()){
            
                produtoId = resultado.getInt("id");
                break;
            
            }
        
        } 
        
        cursor = this.database.createStatement();
        sql = String.format("INSERT INTO produtosDosFornecedores (precoUnitario, produtoId, fornecedor, estoque) VALUES (%s, %s, %s, 0)", preco, produtoId, fornecedorId);
        
        
        cursor.execute(sql);
        
        
        return true;
        
    }
    
    public ArrayList<PedidoFornecedor> listarPedidosFornecedor(String fornecedor) throws SQLException{
        
        ArrayList<PedidoFornecedor> pedidos = new ArrayList();
        
        Statement cursorPedidos = this.database.createStatement();
        ResultSet resultadosPedidos = null;
        ResultSet resultadosProdutos = null;
        String sql;
        
        
        // buscar pedidos do fornecedor
        
        sql = String.format("SELECT\n" +
                            "    clientes.nomeCompleto AS cliente,\n" +
                            "    clientes.email AS contato,\n" +
                            "    clientes.cpfCnpj AS identificador,\n" +
                            "    pedidos.id AS pedido,\n" +
                            "    pedidos.data AS data,\n" +
                            "    enderecos.bairro AS bairro,\n" +
                            "    enderecos.cep AS cep,\n" +
                            "    enderecos.cidade AS cidade, \n" +
                            "    enderecos.logradouro AS logradouro,\n" +
                            "    enderecos.numero AS numero,\n" +
                            "    enderecos.complemento AS complemento, \n" +
                            "    enderecos.estado AS estado\n" +
                            "FROM\n" +
                            "    pedidos\n" +
                            "    INNER JOIN clientes\n" +
                            "        ON pedidos.cliente = clientes.id\n" +
                            "    INNER JOIN enderecos\n" +
                            "        ON pedidos.endereco = enderecos.id\n" +
                            "    INNER JOIN itensDosPedidos\n" +
                            "        ON itensDosPedidos.pedido = pedidos.id\n" +
                            "    INNER JOIN produtosDosFornecedores\n" +
                            "        ON produtosDosFornecedores.id = itensDosPedidos.produto\n" +
                            "    INNER JOIN fornecedores\n" +
                            "        ON produtosDosFornecedores.fornecedor = fornecedores.id\n" +
                                    "WHERE fornecedores.email = '%s'", fornecedor);
        resultadosPedidos = cursorPedidos.executeQuery(sql);
        
        while(resultadosPedidos.next()){
            
            ArrayList<Produto> produtos = new ArrayList();
            
            double total = 0;
            
            Statement cursorProdutos = this.database.createStatement();
            sql = String.format("SELECT\n" +
                  "\n" +
                    "    pedidos.id AS pedido,\n" +
                    "    produtosDosFornecedores.precoUnitario AS valorUnitario,\n" +
                    "    produtos.nome AS produto,\n" +
                    "    itensDosPedidos.quantidade AS quantidade\n" +
                    "\n" +
                    "FROM\n" +
                    "    pedidos\n" +
                    "    INNER JOIN itensDosPedidos ON itensDosPedidos.pedido = pedidos.id\n" +
                    "    INNER JOIN produtosDosFornecedores ON itensDosPedidos.produto = produtosDosFornecedores.id\n" +
                    "    INNER JOIN produtos ON produtosDosFornecedores.produtoId = produtos.id\n" +
                    "\n" +
                    "WHERE pedidos.id = '%s' ", resultadosPedidos.getInt("pedido"));
            
            resultadosProdutos = cursorProdutos.executeQuery(sql);
            
            while(resultadosProdutos.next()){
                
                produtos.add(new Produto(resultadosProdutos.getString("produto"),"","","",resultadosProdutos.getInt("quantidade"), resultadosProdutos.getDouble("valorUnitario"), 0));
                
                total += (resultadosProdutos.getInt("quantidade") * resultadosProdutos.getDouble("valorUnitario"));
                
                
            }
            
            Endereco endereco = new Endereco(resultadosPedidos.getString("logradouro"),
                                             resultadosPedidos.getString("numero"),
                                             resultadosPedidos.getString("complemento"),
                                             resultadosPedidos.getString("cep"),
                                             resultadosPedidos.getString("bairro"),
                                             resultadosPedidos.getString("estado"),
                                             resultadosPedidos.getString("cidade"));
            
            PedidoFornecedor pedido = new PedidoFornecedor(produtos, 
                                   resultadosPedidos.getInt("pedido"),
                                   resultadosPedidos.getDate("data"),
                                   total,
                                   new Cliente(resultadosPedidos.getString("cliente"),
                                               resultadosPedidos.getString("identificador"),
                                               resultadosPedidos.getString("contato")),
                                   endereco);
            
            pedidos.add(pedido);
            
            
            
            
        }
        
        
        
        
        return pedidos;
        
        
    }


}
