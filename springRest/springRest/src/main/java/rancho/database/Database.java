/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rancho.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author frederico
 */
public class Database {
    
    private Connection connection = null;
    private String database;
    private String user;
    private String password;
    private Statement statement;
    
    public Database(){
        
    }
    
    public Database(String database, String user, String password){
        
        this.user = user;
        this.password = password;
        this.database = database;
        
    }
    
    public void Connect() throws SQLException{
   
        this.connection = DriverManager.getConnection(String.format("jdbc:mysql://localhost/%s?user=%s&password=%s", this.database,this.user, this.password));
        
    }
    
    public void setStatement(Statement statement){
        
        this.statement = statement;
        
    }

    public ResultSet executeQuery(){
        
         ResultSet result = null;
         return result;
        
    }
    
    public Statement createStatement() throws SQLException{
        
        return this.connection.createStatement();
        
    }

}
