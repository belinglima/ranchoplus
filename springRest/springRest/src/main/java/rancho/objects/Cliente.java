/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rancho.objects;

/**
 *
 * @author frederico
 */
public class Cliente {
    
    private String Nome;
    private String identificador;
    private String email;

    public Cliente(String Nome, String identificador, String email) {
        this.Nome = Nome;
        this.identificador = identificador;
        this.email = email;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    
    
}
