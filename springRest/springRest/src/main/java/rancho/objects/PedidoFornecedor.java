/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rancho.objects;

import java.sql.Date;
import java.util.ArrayList;

/**
 *
 * @author frederico
 */
public class PedidoFornecedor extends Pedido {
    
    private Endereco endereco;
    private Cliente cliente;
    
    public PedidoFornecedor(ArrayList<Produto> produtos, int id, Date data, double total, Cliente cliente, Endereco endereco){
        
        super(produtos, id, data, total);
        this.cliente = cliente;
        this.endereco = endereco;
        
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    
    
    
}
