//

String.prototype.format = function () {
        var args = [].slice.call(arguments);
        return this.replace(/(\{\d+\})/g, function (a){
            return args[+(a.substr(1,a.length-2))||0];
        });
};


//funções de gerenciamento de cookies

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookie() {
    var user = getCookie("username");
    if (user != "") {
        alert("Welcome again " + user);
    } else {
        user = prompt("Please enter your name:", "");
        if (user != "" && user != null) {
            setCookie("username", user, 365);
        }
    }
}

//função de gerenciamemento de login /logout

function login() {

  var usernameCliente = document.getElementById("usernameCliente").value;
  var passwordCliente = document.getElementById("passwordCliente").value;
  var respostaJson = "";
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      
      var respostaJson = JSON.parse(this.responseText);

      if (respostaJson['status'] == 'ok'){

        setCookie("username",usernameCliente,1);
        setCookie("mode",respostaJson['mode'],1);
        
        window.location = "index.html";

      }

      else {

        alert("login incorreto!");

      }

    }
  };
  
    xhttp.open("GET", "/login?username="+usernameCliente+"&password="+passwordCliente+"&mode=cliente", true);
    xhttp.send();
  
}

function logout(){
    
    setCookie("mode","",1323);
    window.location = "index.html";
    
}

//definir se o usuario esta logado ou nao

function checarStatus(){
    
    var mode = getCookie("mode");
    
    if (mode == ""){
        
        document.getElementById("login").style.display = "inline";
        document.getElementById("logout").style.display = "none";
        document.getElementById("car").style.display = "none";
        document.getElementById("cadastro").style.display = "inline";
        document.getElementById("lista_produto").style.display = "none";
        document.getElementById("lista_pedidos").style.display = "none";
        
    } else if (mode == "cliente"){
        
        document.getElementById("login").style.display = "none";
        document.getElementById("logout").style.display = "inline";
        document.getElementById("car").style.display = "inline";
        document.getElementById("cadastro").style.display = "none";
        document.getElementById("lista_produto").style.display = "none";
        document.getElementById("lista_pedidos").style.display = "inline";
        
    } else if (mode == "fornecedor"){
        
        document.getElementById("login").style.display = "none";
        document.getElementById("logout").style.display = "inline";
        document.getElementById("car").style.display = "none";
        document.getElementById("cadastro").style.display = "none";
        document.getElementById("lista_produto").style.display = "inline";
        document.getElementById("lista_pedidos").style.display = "inline";
        
    }
    
}

//checar status

try {
 
    checarStatus();
    
} catch(error) {
    
}


// popular lista de produtos da pagina inicial

function listarProdutos(){

    var produtos = document.getElementById("produtos");

    var template = "<div class='col-md-4' style='float:false'>";
    template +=         "<div class='panel panel-primary'>";
    template +=             "<div class='panel-heading'>{0}</div>";
    template +=             "<div class='panel-body' style='width:75%'><img src='{1}' style='width:150px' alt='Image'></div>";
    template +=             "<div class='panel-footer' style='text-align:center'><a href='produto.html?id={4}'>R$ {2} ({3})</a></div>";
    template +=         "</div>";
    template +=     "</div>";

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {

            var respostaJson = JSON.parse(this.responseText);
            
            var counter = 0;
            
            for (var i = 0; i < respostaJson.length; i++){
                
                var produto = respostaJson[i];
                
                if (i % 3 === 0){
                    
                    
                produtos.innerHTML += '<div class="row">';    
                   
                    
                }
                
                
                produtos.innerHTML += template.format(produto['nome'], produto['fotoURL'],
                                                    parseFloat(produto['valor']).toFixed(2),
                                                    produto['fornecedor'], produto['idProdutoDoFornecedor']);
                
                if (i % 3 === 0){
                    
                    produtos.innerHTML += '</div>';    
                   
                    
                }
            }
            
        };
    
    };
  
    xhttp.open("GET", "produtos?numero=100");
    xhttp.send();
    
}

try {
 
    document.getElementById("index-body").addEventListener("load", listarProdutos());
    
} catch(error) {
   
    
}

// cadadastrar usuario

function cadastrarUsuario(){
    
var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {

            var respostaJson = JSON.parse(this.responseText);
            
            alert(respostaJson['status']);
            
            if(respostaJson['status'] == 'ok'){
                
                alert("cadastro efetuado com sucesso!");
                
            } else {
                
                alert("erro ao efetuar o cadastro");
                
            }
            
        };
    
    };
  
    var URL = "";
    
    if (document.getElementById("Cli").checked){
        
        URL = "cadastro_usuario_cliente?";
        URL += "username=" + encodeURIComponent(document.getElementById("usuario-cliente").value) ;
        URL += "&email=" + encodeURIComponent(document.getElementById("email-cliente").value) ;
        URL += "&password=" + encodeURIComponent(document.getElementById("password-cliente").value) ;
        URL += "&nome_completo=" + encodeURIComponent(document.getElementById("nome-cliente").value) ;
        URL += "&cpf_cnpj=" + encodeURIComponent(document.getElementById("cpfcnpj-cliente").value) ;
        URL += "&rg=" + encodeURIComponent(document.getElementById("rg-cliente").value) ;
        URL += "&data_nascimento=" + encodeURIComponent(document.getElementById("nascimento-cliente").value) ;
        URL += "&tipo=" + encodeURIComponent(document.getElementById("tipo-cliente").value) ;
        
    } else if (document.getElementById("For").checked){
        
        URL = "cadastro_usuario_fornecedor?";
        URL += "username=" + encodeURIComponent(document.getElementById("usuario-fornecedor").value) ;
        URL += "&email=" + encodeURIComponent(document.getElementById("email-fornecedor").value) ;
        URL += "&password=" + encodeURIComponent(document.getElementById("password-fornecedor").value) ;
        URL += "&nome_completo=" + encodeURIComponent(document.getElementById("razao-fornecedor").value) ;
        URL += "&cpf_cnpj=" + encodeURIComponent(document.getElementById("cnpj-fornecedor").value) ;
        
    } else {
        
        return false;
        
    }
    
  
    xhttp.open("GET", URL);
    xhttp.send();    
    
}

try {
    
    document.getElementById("submeter").addEventListener("click", cadastrarUsuario());
    
} catch (error){
    
    
}


// recuperar dados do produto

function recuperarDadosProduto(){
        
    var produtoId = window.location.href.split('?')[1].split("id=")[1];
    var dadosProduto = document.getElementById("produto");
    var dadosFornecedor = document.getElementById("fornecedor");
    var dadosValor = document.getElementById("valor");
    var dadosURL = document.getElementById("fotoURL");
    var Produto = document.getElementById("produto-id");
    var cliente = document.getElementById("cliente-id");
    cliente.value = getCookie("username");
    
    if (produtoId == "undefined" || produtoId == ""){
        
        window.location.href = "index.html";
        
    }

    var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState === 4 && this.status === 200) {

                if (this.responseText == ""){
                    
                    window.location.href = "index.html";
                    
                };

                var respostaJson = JSON.parse(this.responseText);
                                
                dadosProduto.textContent = respostaJson['nome'];
                dadosFornecedor.textContent = respostaJson['fornecedor'];
                dadosValor.textContent = respostaJson['valor'];
                dadosURL.src = respostaJson['fotoURL'];
                Produto.value = respostaJson['idProdutoDoFornecedor'];
                
                
                
                
            };

        };

        xhttp.open("GET", "produtoDados?id={0}".format(produtoId));
        xhttp.send();
}

function adicionarAoCarrinho(){
    
    var cliente = document.getElementById("cliente-id");
    var produto = document.getElementById("produto-id");
    var quantidade = document.getElementById("produto-quantidade");

    var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState === 4 && this.status === 200) {
                
                var respostaJson = JSON.parse(this.responseText);


                if(respostaJson['status'] == 'ok'){

                    alert("produto adicionado ao carrinho");

                } else {

                    alert("erro ao adicionar o produto");

                }

            };

        };
    
    xhttp.open("GET", "adicionarAoCarrinho?cliente={0}&produto={1}&quantidade={2}".format(cliente.value, produto.value, quantidade.value));
    xhttp.send();
    
}

try {
    
    document.getElementById("produto-body");
    recuperarDadosProduto();
    document.getElementById("adicionar").addEventListener("click", adicionarAoCarrinho);
   
        if (getCookie("mode") == "cliente"){
        
        document.getElementById("adicionar").disabled = false;
        
    } else {
        
        document.getElementById("adicionar").disabled = true;
        
    }
    
} catch (error){

    
}

// recuperar dados, efetuar compra ou limpar carrinho

function mostrarProdutoCarrinho(){
    
    var cliente = getCookie("username");
    var tabela = document.getElementById("table-data");

    var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState === 4 && this.status === 200) {
                
                var respostaJson = JSON.parse(this.responseText);
                alert(respostaJson);
                
                respostaJson.forEach(function (element) {
                    
                    tabela.innerHTML += "<td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td>".format(element['nome'], 
                                                                                                  element['fornecedor'], 
                                                                                                  element['quantidade'],
                                                                                                  element['valor']);
                
                });
                    
                
                }

            };

    
    xhttp.open("GET", "mostrarProdutoCarrinho?username={0}".format(cliente));
    xhttp.send();    
    
}

function limparCarrinho(){
    
    var cliente = getCookie("username");

    var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState === 4 && this.status === 200) {
                
                window.location.href = "/meu_carrinho.html";

            }
        }
        
    xhttp.open("GET", "limparCarrinho?username={0}".format(cliente));
    xhttp.send();     
    
}

function efetuarCompra(){
    
    window.location.href = "pedido.html";
    
}

function mostrarProdutoPedido(){
    
    var cliente = getCookie("username");
    var tabela = document.getElementById("table-data-pedido");

    var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState === 4 && this.status === 200) {
                
                var respostaJson = JSON.parse(this.responseText);
                
                respostaJson.forEach(function (element) {
                    
                    tabela.innerHTML += `<div class="col-sm-7">
                                             <div class="form-group">
                                                 <input type="text" class="form-control" value="{0} tall" name="nome" readonly>
                                             </div>
                                        </div>
                                        <div class="col-sm-3">
                                        <div class="form-group">
                                          <input type="text" class="form-control" value="{1} tall"  name="fornecedor" readonly>
                                        </div>
                                        </div>
                                        <div class="col-sm-2">
                                        <div class="form-group"> 
                                        <input type="text" class="form-control" value={2} name="qtd" id="qtd" readonly>
                                        </div>
                                        </div>  `.format(element['nome'], 
                                                                                                  element['fornecedor'], 
                                                                                                  element['quantidade'],
                                                                                                  element['valor']);
                
                });
                    
                
                }

            };

    
    xhttp.open("GET", "mostrarProdutoCarrinho?username={0}".format(cliente));
    xhttp.send();    
    
}

function mostrarProdutoCarrinho(){
    
    var cliente = getCookie("username");
    var tabela = document.getElementById("table-data");

    var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState === 4 && this.status === 200) {
                
                var respostaJson = JSON.parse(this.responseText);
                
                respostaJson.forEach(function (element) {
                    
                    try {
                    
                        tabela.innerHTML += `<td>{0}</td><td>{1}</td> <td>{2}</td> <td>{3}</td> `.format(element['nome'], 
                                                                                                  element['fornecedor'], 
                                                                                                  element['quantidade'],
                                                                                                  element['valor']);
                                                                                              } catch (error) {
                                                                                              };
                
                });
                    
                
                }

            };

    
    xhttp.open("GET", "mostrarProdutoCarrinho?username={0}".format(cliente));
    xhttp.send();    
    
}


try {
    
    document.getElementById("carrinho-body");
    mostrarProdutoCarrinho();
    document.getElementById("efetuar-compra").addEventListener("click", efetuarCompra);
    document.getElementById("limpar-carrinho").addEventListener("click", limparCarrinho);
   
        if (getCookie("mode") == "cliente"){
        
        document.getElementById("adicionar").disabled = false;
        
    } else {
        
        document.getElementById("adicionar").disabled = true;
        
    }
    
} catch (error){

}


// cofirmar pedido

function confirmarPedido(){
    
    var cliente = getCookie("username");
    var cep = document.getElementById("cep").value;
    var bairro = document.getElementById("bairro").value;
    var rua = document.getElementById("rua").value;
    var numero = document.getElementById("numero").value;
    var uf = document.getElementById("uf").value;
    var cidade = document.getElementById("cidade").value;
    var complemento = document.getElementById("complemento").value;
    
    var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState === 4 && this.status === 200) {
                
                    var respostaJson = JSON.parse(this.responseText);
                    
                    alert(respostaJson['status']);
                
                }

            };

    
    xhttp.open("GET", "confirmarPedido?username={0}&cep={1}&rua={2}&numero={3}&uf={4}&cidade={5}&complemento={6}&bairro={7}".format(cliente, cep, rua, numero, uf, cidade, complemento, bairro));
    
    xhttp.send();    
    
}

try {
    document.getElementById("confirmar-pedido-body");
    document.getElementById("botao-confirmar").addEventListener('click',confirmarPedido);
    mostrarProdutoPedido();

    
} catch (error){

}

// listar pedidos

function listarPedidos(){
   
    
    var cliente = getCookie("username");
    var tabela = document.getElementById("tabela-meus-pedidos");
    var templateProduto = `<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td></tr>`;
    var templateDiv = `<div class="row">
    
    <div class="col-sm-12">
        <h4>Pedido {0} ({1})</h4>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Produto</th>
                    <th>Fornecedor</th>
                    <th>Quantidade</th>
                    <th>Valor</th>
                    <th></th>
                </tr>
            </thead>
            <tbody id="table-data">
                {2}
            </tbody>
        </table>
        <h3>Total: R$ {3}</h3></div>`;
    
    var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            
            if (this.readyState === 4 && this.status === 200) {
               
                
                    var respostaJson = JSON.parse(this.responseText);
                    
                    respostaJson.forEach(function (pedido) {
                        
                        
                        var linhas_produto = ""
                        
                        pedido['produtos'].forEach(function(produto){
                            
                            linhas_produto += templateProduto.format(produto['nome'], 
                                                                     produto['fornecedor'], 
                                                                     produto['quantidade'], 
                                                                     parseFloat(produto['valor']).toFixed(2));
                            
                        })
                        
                        
                        tabela.innerHTML += templateDiv.format(pedido['id'], 
                                                               pedido['data'], 
                                                               linhas_produto, 
                                                               parseFloat(pedido['total']).toFixed(2));
                        
                        
                    })
                
                }

            };

    
    xhttp.open("GET", "listarPedidos?username={0}".format(cliente));
    xhttp.send();
    
    
    
}

try {
    listarPedidos();
} catch (error) {

}

// listar produtos do fornecedor

function listarMeusProdutos(){
    
    var cliente = getCookie("username");
    var tabela_meus_produtos = document.getElementById("tabela-meus-produtos");
    
    if(tabela_meus_produtos == null){
        
        return;
        
    }
    
    var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            
            if (this.readyState === 4 && this.status === 200) {
               
                
                    var respostaJson = JSON.parse(this.responseText);
                    
                    
                    for (var i = 0; i < respostaJson.length; i++){
                        
                        tabela_meus_produtos.innerHTML += `<tr><td>{0}</td><td>{1}</td><td>{2}</td><td><a onclick='deletarProduto({3})' href="">deletar</a></td></tr>`.format(respostaJson[i]['categoria'],
                        
                                                                                                                 respostaJson[i]['nome'],
                                                                                                                 respostaJson[i]['valor'],
                                                                                                                 respostaJson[i]['idProdutoDoFornecedor']);
                        
                    }
                
                }

            };

    
    xhttp.open("GET", "listarMeusProdutos?fornecedor={0}".format(cliente));
    xhttp.send();    
}

try {
    
    listarMeusProdutos();
    
    
    
} catch (error){
    
}

// deletar produto

function deletarProduto(id){
    
    var fornecedor = getCookie("username");
    
    var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            
            if (this.readyState === 4 && this.status === 200) {
               
                
                    var respostaJson = JSON.parse(this.responseText);
                    
                    if (respostaJson['status'] == 'ok'){
                        
                        alert("produto removido");
                        
                    } else {
                        
                        alert("erro ao remover produto");
                        
                    }
                    
                
                }

            };

    
    xhttp.open("GET", "deletarProduto?id={0}&fornecedor={1}".format(id,fornecedor));
    
    xhttp.send();       
    
}

try {
    
} catch (error){
    
}

// cadastrar produto

function cadastrarProduto(){
    
    var fornecedor = getCookie("username");
    var produto_nome = document.getElementById("nome").value;
    var produto_identificador = document.getElementById("identificador").value;
    var produto_preco = document.getElementById("preco").value;
    var produto_foto = document.getElementById("foto").value;
    
    var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            
            if (this.readyState === 4 && this.status === 200) {
               
                
                    var respostaJson = JSON.parse(this.responseText);
                    
                    if (respostaJson['status'] == 'ok'){
                        
                        alert("produto cadastrado");
                        
                    } else {
                        
                        alert("erro ao cadastrar produto");
                        
                    }
                    
                
                }

            };

    
    xhttp.open("GET", "cadastrarProduto?fornecedor={0}&nome={1}&identificador={2}&preco={3}&fotoURL={4}".format(fornecedor,produto_nome, produto_identificador, produto_preco, produto_foto));
    
    xhttp.send();       
    
}


try {
    
    var button = document.getElementById("cadastrar-produto-btn");
    button.addEventListener("click", cadastrarProduto);
    
} catch (error){
    
}

// ver pedidos

function listarPedidosFonecedor(){

    var fornecedor = getCookie("username");
    var tabelaPedidos = document.getElementById('lista-pedidos-fornecedor');
    
    var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            
            var rowTemplate = `<tr>
                                    <td>Nome: {0}<br>
                                        Identificador: {1}<br>
                                        <br>
                                    
                                        </td>
                                    <td>E-mail: {2}</td>
                                    <td>{3}</td>
                                    <td>R$ {4}</td>
                                    <td>{5}</td>
                                </tr>`;
            
            var produtoTemplate = `{0} x {1}<br>`;
            
            var enderecoTemplate = `Rua: {0}<br>
                                    Numero: {1}<br>
                                    Complemento: {2}<br>
                                    Bairro: {3}<br>
                                    Cidade: {4} ({5})<br>
                                    CEP: {6}`;
            
            
            if (this.readyState === 4 && this.status === 200) {
               
                
                    var respostaJson = JSON.parse(this.responseText);
                    
                    for(var i = 0; i < respostaJson.length; i++){
                        
                        var pedido = respostaJson[i];
                        
                        var produtos = "";
                        
                        for(var j = 0; j < pedido['produtos'].length; j++){
                            
                            var produto = respostaJson[i]['produtos'][j];
                            
                            //alert(JSON.stringify(produto));
                            
                            produtos += produtoTemplate.format(produto['quantidade'], 
                                                               produto.nome);
                            
                        };
                        
                        var row = rowTemplate.format(pedido.cliente.nome,
                                                     pedido.cliente.identificador,
                                                     pedido.cliente.email,
                                                     produtos,
                                                     parseFloat(pedido.total).toFixed(2),
                                                     enderecoTemplate.format(pedido.endereco.logradouro,
                                                                             pedido.endereco.numero,
                                                                             pedido.endereco.complemento,
                                                                             pedido.endereco.bairro,
                                                                             pedido.endereco.cidade,
                                                                             pedido.endereco.uf,
                                                                             pedido.endereco.cep));
                                                     
                                                     
                        tabelaPedidos.innerHTML += row;
                        
                        
                        
                    }
                    
                    
                    
                    
                
                }

            };

    
    xhttp.open("GET", "PedidosFornecedor?fornecedor={0}".format(fornecedor));
    
    xhttp.send();  
    
}

try {
    
    document.getElementById("lista-pedidos-fornecedor");
    listarPedidosFonecedor();
    
} catch (error){
    
    alert(error);
    
}