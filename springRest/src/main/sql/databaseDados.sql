-- selecionar banco de dados

use mydb;

-- cadastrar clientes

INSERT INTO clientes (username, email, password, nomeCompleto, cpfCnpj , tipoDePessoa) VALUES 
	('fred',  'fred.s.kremer@gmail.com', '123',  'frederico kremer', '2131231',  'fisica'),
	('joca',  'joca@pop.com.br',         '123',  'joca martins',     '21290555', 'fisica'),
	('mike',  'mike@xvideos.com',        '123',  'mike tyson',       '21290555', 'fisica'),
	('bob',   'esponja@siri.com',        '123',  'bob esponja',      '21290555', 'fisica'),
	('zack',  'zack@zack.zack',          '123',  'zack snyder',      '21290555', 'fisica'),
	('marcus',  'mar@cu.s',              '123',  'marcus cunha',     '21290555', 'fisica');

-- cadastrar fornecedores

INSERT INTO fornecedores (username, email, password, razaoSocial, cnpj) VALUES 
	('guanabara',  'contato@guanabara.com', '123',  'Guanabara LTA',   '2131231'),
	('big',        'contato@big.com.br',    '123',  'Big S.A.',        '21290555'),
	('nacional',   'contato@nacional.com',  '123',  'Nacional Super',  '21290555');

-- cadastrar categorias de produtos

INSERT INTO produtos (nome, identificadorUniversal, fotoURL) VALUES 
	('coca-cola 2 litros',  '12345678', 'https://mambo.vteximg.com.br/arquivos/ids/211087/135707.jpg?v=636366880526730000'),
	('pepsi-cola 2 litros', '32134123', 'https://cdn-cosmos.bluesoft.com.br/products/7892840800000'),
	('guarana 2 litros',    '3123312321', 'https://mambo.vteximg.com.br/arquivos/ids/198676/135299.jpg?v=636095443012870000');


-- -----------------------------------------------------
-- Table `mydb`.`produtosDosFornecedores`
-- -----------------------------------------------------

INSERT INTO produtosDosFornecedores (precoUnitario, produtoId, fornecedor, estoque) VALUES (2.52, 1, 1, 10),
                                                                                           (2.14, 2, 1, 10),
                                                                                           (2.87, 3, 1, 10);


INSERT INTO produtosDosFornecedores (precoUnitario, produtoId, fornecedor, estoque) VALUES (5.52, 1, 2, 10),
                                                                                           (5.14, 2, 2, 10),
                                                                                           (4.87, 3, 2, 10);

INSERT INTO produtosDosFornecedores (precoUnitario, produtoId, fornecedor, estoque) VALUES (3.52, 1, 3, 10),
                                                                                           (6.14, 2, 3, 10),
                                                                                           (1.87, 3, 3, 10);



