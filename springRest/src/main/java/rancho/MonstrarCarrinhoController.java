/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rancho;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author frederico
 */


@RestController
public class MonstrarCarrinhoController {

    
    @RequestMapping("/mostrarProdutoCarrinho")
    public ArrayList<Produto> mostrarProdutoCarrinho(@RequestParam(value="username", defaultValue="") String username) throws FileNotFoundException, SQLException {
                
        DatabaseOperator database = new DatabaseOperator();
        
        return database.buscarProdutosCarrinho(username);
        
    
    }

    @RequestMapping("/limparCarrinho")
    public Cadastro limparCarrinho(@RequestParam(value="username", defaultValue="") String username) throws FileNotFoundException, SQLException {
                
        DatabaseOperator database = new DatabaseOperator();
        
        return database.limparCarrinho(username);
        
    
    }

    @RequestMapping("/confirmarPedido")
    public Cadastro confirmarPedido(@RequestParam(value="username", defaultValue="") String username,
                                    @RequestParam(value="cep", defaultValue="") String cep,
                                    @RequestParam(value="bairro", defaultValue="") String bairro,
                                    @RequestParam(value="rua", defaultValue="") String rua,
                                    @RequestParam(value="cidade", defaultValue="") String cidade,
                                    @RequestParam(value="uf", defaultValue="") String uf,
                                    @RequestParam(value="numero", defaultValue="") String numero,
                                    @RequestParam(value="complemento", defaultValue="") String complemento) throws FileNotFoundException, SQLException {
                
        DatabaseOperator database = new DatabaseOperator();
        
        try {
        
            int enderecoId = database.cadastrarEndereco(uf, cidade, cep, rua, numero, complemento, bairro);
            int clienteId  = database.clienteId(username);
            boolean resposta = database.confirmarPedido(clienteId, enderecoId);
                                    
            if(resposta){

                return new Cadastro("ok", "pedido cadastrado!");                
                
            } else {
                
                return new Cadastro("error", "erro ao confirmar pedido");
                
            }
            
            
        } catch (SQLException ex){

            return new Cadastro("error", "erro ao confirmar pedido");
            
        }
        
    
    }

    @RequestMapping("/mostrarPedidos")
    public void mostrarPedido(@RequestParam(value="username", defaultValue="") String username){
        
        
        
    }

}
