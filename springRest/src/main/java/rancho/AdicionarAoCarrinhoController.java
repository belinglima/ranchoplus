package rancho;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;

@RestController
public class AdicionarAoCarrinhoController {    

    @RequestMapping("/adicionarAoCarrinho")
    public Cadastro adicionarAoCarrinho(@RequestParam(value="cliente", defaultValue="") String cliente,
                                     @RequestParam(value="produto", defaultValue="") String produto,
                                     @RequestParam(value="quantidade", defaultValue="") String quantidade) throws FileNotFoundException, SQLException {
                
        DatabaseOperator database = new DatabaseOperator();
        
        if(database.adicionarProdutoAoCarrinho(cliente, produto, quantidade)){
            
            return new Cadastro("ok", "produto adicionado com sucesso");
            
        } else {
            
            return new Cadastro("erro", "erro ao adicionar o produto");
        }
        
    }
}