/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rancho;

import java.time.LocalDate;

/**
 *
 * @author frederico
 */
public class UsuarioCliente extends Usuario {
    
    private String nomeCompleto;
    private String cpf;
    private String rg;
    private LocalDate dataDeNascimento;

    public UsuarioCliente(String nomeCompleto, String cpf, String rg, LocalDate dataDeNascimento, int id, String username, String email, String password, int endereco) {
        super(id, username, email, password, cpf, endereco);
        this.nomeCompleto = nomeCompleto;
        this.setIdentificador(cpf);
        this.cpf = cpf;
        this.rg = rg;
        this.dataDeNascimento = dataDeNascimento;
        
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public LocalDate getDataDeNascimento() {
        return dataDeNascimento;
    }

    public void setDataDeNascimento(LocalDate dataDeNascimento) {
        this.dataDeNascimento = dataDeNascimento;
    }
    
    
    
    
}
