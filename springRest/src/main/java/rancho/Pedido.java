/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rancho;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author frederico
 */
public class Pedido {
    
    private ArrayList<Produto> produtos;
    private int id;
    private Date data;
    private double total;

    public Pedido(ArrayList<Produto> produtos, int id, Date data, double total) {
        this.produtos = produtos;
        this.id = id;
        this.data = data;
        this.total = total;
    }

    public ArrayList<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(ArrayList<Produto> produtos) {
        this.produtos = produtos;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
    
    public void addProduto(Produto produto){
        
        this.produtos.add(produto);
        
    }
    
    
    
    
    
}
