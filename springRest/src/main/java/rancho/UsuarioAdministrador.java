/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rancho;

import java.time.LocalDate;

/**
 *
 * @author frederico
 */
public class UsuarioAdministrador extends Usuario {
    
    private String nomeCompleto;
    private String rg;
    private String cpf;
    private LocalDate dataDeNascimento;
    private String cargo;
    private String setor;

    public UsuarioAdministrador(String nomeCompleto, String rg, String cpf, LocalDate dataDeNascimento, String cargo, String setor, int id, String username, String email, String password, String identificador, int endereco) {

        super(id, username, email, password, identificador, endereco);
        this.nomeCompleto = nomeCompleto;
        this.rg = rg;
        this.cpf = cpf;
        this.dataDeNascimento = dataDeNascimento;
        this.cargo = cargo;
        this.setor = setor;
    }
    
    
            
}
