package rancho;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;

@RestController
public class LoginController {

    private final AtomicLong counter = new AtomicLong();
    

    @RequestMapping("/login")
    public Login login(@RequestParam(value="username", defaultValue="") String username,
                                     @RequestParam(value="password", defaultValue="") String password,
                                     @RequestParam(value="mode", defaultValue="") String mode) throws FileNotFoundException, SQLException {
                
        DatabaseOperator database = new DatabaseOperator();
        
        if (database.checarCadastroAdministrador(username, password)){
            
            return new Login("ok", "login verificado", "administrador");
            
        } else if (database.checarCadastroCliente(username, password)){
            
            return new Login("ok", "login verificado", "cliente");
            
        } else if (database.checarCadastroFornecedor(username, password)){
            
            return new Login("ok", "login verificado", "fornecedor");
            
        } else {
            
            return new Login("error", "email não cadastrado", "");
            
        }
        
    }
}