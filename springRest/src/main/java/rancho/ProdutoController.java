package rancho;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
public class ProdutoController {

    private final AtomicLong counter = new AtomicLong();
    
    //rota para cadastrar um cliente
    
    @RequestMapping("/produtos")
    public ArrayList<Produto> topPprodutos(@RequestParam(value="numero", defaultValue="8") int numero) {
                
        DatabaseOperator database = null;
        try {
            database = new DatabaseOperator();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProdutoController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ProdutoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            return database.buscarProdutos(numero);
        } catch (SQLException ex) {
            Logger.getLogger(ProdutoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
        
    }
     
    @RequestMapping("/produtoDados")
    public Produto DadosProdutos(@RequestParam(value="id", defaultValue="") int id){
        
        DatabaseOperator database = null;
        Produto produto = null;
        
        try {
            
            database = new DatabaseOperator();
            
            return database.buscarProduto(id);
            
        } catch (FileNotFoundException ex) {
            
            return null;
            
        } catch (SQLException ex){
            
            return null;
            
        }
   
    }

}