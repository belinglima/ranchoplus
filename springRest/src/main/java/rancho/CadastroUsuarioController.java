package rancho;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;

@RestController
public class CadastroUsuarioController {

    private final AtomicLong counter = new AtomicLong();
    
    //rota para cadastrar um cliente
    
    @RequestMapping("/cadastro_usuario_cliente")
    public Cadastro cadastro_usuario_cliente(@RequestParam(value="username", defaultValue="") String usuario,
                       @RequestParam(value="email", defaultValue="") String email,
                       @RequestParam(value="password", defaultValue="") String password,
                       @RequestParam(value="nome_completo", defaultValue="") String nome_completo,
                       @RequestParam(value="cpf_cnpj", defaultValue="") String cpf_cnpj,
                       @RequestParam(value="rg", defaultValue="") String rg,
                       @RequestParam(value="data_nascimento", defaultValue="") String data_nascimento,
                       @RequestParam(value="tipo", defaultValue="") String tipo) throws FileNotFoundException, SQLException {
         
        
        DatabaseOperator database = new DatabaseOperator();
        
        if (database.usernameCadastrado(usuario)){
            
            return new Cadastro("erro", "username já cadastrado!");
            
        }
        
        if (database.emailCadastrado(email)){
            
            return new Cadastro("erro", "e-mail já cadastrado!");
            
        } else {
            
            /**
             * String usuario
             * String email
             * String password, String nome
             * String cpfCnpj
             * String rg
             * String tipo
             **/
        
            if (database.cadastrarCliente(usuario, email, password, nome_completo, cpf_cnpj, rg, tipo)){
                
                return new Cadastro("ok", "fornecedor cadastrado!");
                
            } else {
                
                return new Cadastro("erro", "erro ao efetuar cadastro");
                
            }
        
        }
        
    }


    //rota para cadastrar um fornecedores
    
    @RequestMapping("/cadastro_usuario_fornecedor")
    public Cadastro cadastro_usuario_fornecedor(@RequestParam(value="username", defaultValue="") String usuario,
                       @RequestParam(value="email", defaultValue="") String email,
                       @RequestParam(value="password", defaultValue="") String password,
                       @RequestParam(value="razao", defaultValue="") String razao_social,
                       @RequestParam(value="cpf_cnpj", defaultValue="") String cpf_cnpj) throws FileNotFoundException, SQLException {
                
        DatabaseOperator database = new DatabaseOperator();
        
        if (database.emailCadastrado(email)){
            
            return new Cadastro("erro", "e-mail já cadastrado!");
            
        } else  {
            
            /**
             * sString usuario, 
             * String email, 
             * String password, 
             * String razaoSocial,
             * String cpfCnpj
             **/
        
            if (database.cadastrarFornecedor(usuario, email, password, razao_social, cpf_cnpj)){
                
                return new Cadastro("ok", "fornecedor cadastrado!");
                
            } else {
                
                return new Cadastro("erro", "erro ao efetuar cadastro");
                
            }
        
        }
        
    }


}