/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rancho;

/**
 *
 * @author frederico
 */
public class Produto {
    
    private String nome;
    private String fornecedor;
    private String categoria;
    private String fotoURL;
    private int quantidade;
    private double valor;
    private int idProdutoDoFornecedor;

    public Produto(String nome, String fornecedor, String fotoURL, String categoria, int quantidade, double valor, int idProdutoDoFornecedor) {

        this.nome = nome;
        this.fornecedor = fornecedor;
        this.categoria = categoria;
        this.quantidade = quantidade;
        this.valor = valor;
        this.fotoURL = fotoURL;
        this.idProdutoDoFornecedor = idProdutoDoFornecedor;
    }

    public String getFotoURL() {
        return fotoURL;
    }

    public void setFotoURL(String fotoURL) {
        this.fotoURL = fotoURL;
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(String fornecedor) {
        this.fornecedor = fornecedor;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public int getIdProdutoDoFornecedor() {
        return idProdutoDoFornecedor;
    }

    public void setIdProdutoDoFornecedor(int idProdutoDoFornecedor) {
        this.idProdutoDoFornecedor = idProdutoDoFornecedor;
    }
    
    
    
}
