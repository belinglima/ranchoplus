# Rancho+: Um sistema online para compras em supermercados

## Objetivos

Desenvolvimento de um sistema de compras *online* em supermercados que permita a
aquisição em diferentes fornecedores, de forma similar aos marketplaces de sites 
como Amazon.

## Tecnologias

- Sistema desenvolvido em linguagem **Java** para o **back-end**
- **Front-end** usando **HTML5**
- **MySQL** como sistema de gerenciamento de banco de dados
- Arquitetura **RESTFul**
- Uso do **Maven** para gerenciamento das dependências na aplicação
- Versionamente usando **Github**

## Análise de requisitos

### Requisitos Funcionais
```
RF0001 - cadastrar cliente 
RF0002 - cadastrar fornecedor
RF0003 - cadastrar produtos (fornecedor)
RF0004 - visualizar produtos de diferentes fornecedores (cliente)
RF0005 - efetuar pedido (cliente)
RFOOO6 - receber pedidos (fornecedor)
```

### Requisitos não  Funcionais
```
RNF0001 – design responsivo. 
RNF0002 – segurança (uso de senhas para acesso)
RNF0003 – sistema 24 horas online 
RNF0004 – sistema guarda registros no banco de dados
RNF0005 – não é permitido apagar extrato de compras
RNF0006 – para realizar compra, necessário cadastro.
```

## Diagrama ER do banco de dados

![Diagrama ER](modelo.jpg)
